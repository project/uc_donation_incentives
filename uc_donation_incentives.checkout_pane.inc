<?php

/*
 * callback for hook_checkout_pane()
 */
function uc_donation_incentives_checkout_pane_donations($op, &$arg1, $arg2){
  switch ($op) {
    case 'view':
      if(isset($arg1) && isset($arg1->donation_incentive->rebated)) {
        $donation_incentive = $arg1->donation_incentive->rebated;
      } else {
        $donation_incentive = uc_donation_incentives_calc_cart_incentive();
      }
      // pass the calculated incentive through the pane form
      $contents['donation_calculated'] = array(
        '#type' => 'value',
        '#value' => $donation_incentive,
      );
      // presentation of incentive
      if($donation_incentive) {
        $contents['donation_incentive'] = array(
          '#value' => theme('uc_donation_incentives_donations_preview', $donation_incentive),
        );
      } else {
        $contents = array();
      }

      // add form for additional donation
      if(variable_get('uc_donation_incentives_topup', FALSE)) {
        $contents['donation_additional'] = array(
          '#title' => t('Top-Up Donation'),
          '#description' => t('You can optionally add an additional donation amount to your purchase.'),
          '#type' => 'textfield',
          '#default_value' => (isset($arg1) && isset($arg1->donation_incentive->purchased)) ? check_plain(uc_currency_format($arg1->donation_incentive->purchased, FALSE)) : ''
        );
      }
      // js for updating line item
      drupal_add_js(drupal_get_path('module', 'uc_donation_incentives') .'/uc_donation_incentives.js');
      
      return array('description' => '', 'contents' => $contents);

    case 'process':
      // we validate the form here (arg2). Returning FALSE will cause the order not to process
      if(isset($arg2['donation_additional']) && $arg2['donation_additional']) {
        $pattern = '/^\d*(\.\d*)?$/';
        $price_error = t('Additional Donation amount must be in a valid number. No commas and only one decimal point.');

        if (!is_numeric($arg2['donation_additional']) && !preg_match($pattern, $arg2['donation_additional'])) {
          form_set_error('donation_additional', $price_error);
          return FALSE;
        }
        $arg1->donation_incentive->purchased = $arg2['donation_additional'];
      }
      //ensure order object has the calculated incentive (needed when updating existing order)
      $arg1->donation_incentive->rebated = $arg2['donation_calculated'];
      return TRUE;

    case 'review':
      $review = array();
      if($arg1->donation_incentive->rebated){
        $review[] = array('title' => t('Included Donation'), 'data' => check_plain(uc_currency_format($arg1->donation_incentive->rebated)));
      } else {
        $review[] = array('title' => t('Included Donation'), 'data' => t('none'));
      }
      if(isset($arg1->donation_incentive->purchased) && $arg1->donation_incentive->purchased){
        $review[] = array('title' => t('Top-Up Donation'), 'data' => check_plain(uc_currency_format($arg1->donation_incentive->purchased)));
      }
      return $review;

    case 'settings':
      $form['uc_donation_incentives_topup'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow customers to add an additional donation amount to their order.'),
        '#default_value' => variable_get('uc_donation_incentives_topup', FALSE),
      );
      return $form;
  }
}
