UC_DONATION_INCENTIVES README

###############################################################################
# SYNOPSIS
###############################################################################

Ubercart Donation Incentives allows a store owner to apply donation incentives
to products. These incentives can be thought of as rebates on the purchase, with
the rebate being a donation amount. The donation incentives can either be a
fixed amount, or a percentage of the sell price. A default donation incentive
can be applied to all products, and can also be set on a per-product basis.

###############################################################################
# FUNCTIONALITY SCOPE
###############################################################################

This module only applies donation incentives to products. Charity selection,
donation remittance and tax receipting is to be handled either manually or by
an external module.

Manually: You make donations on your customers' behalf, or send your customer
a donation gift e-card (available at several sites).

External module: Drupal modules available from Benevity can be used to
allow your customers to select their favorite charities, track donations, and
obtain tax receipts right within your Ubercart store. See http://www.benevity.org

###############################################################################
# REQUIREMENTS
###############################################################################

 * Drupal 6.x
 * Ubercart 2.x

###############################################################################
# INSTALLATION
###############################################################################

 1. Copy the uc_donation_incentives directory to your module directory
   (ie: sites/all/modules)

 2. Enable "Donation Incentives" in Drupal's module admin

 3. There are several areas where you control the display of donation incentives.
    It's recommended that you ensure all of the following fields are enabled:
     * "Donation Incentives" @ admin/store/settings/cart/edit/panes
     * "Donation" @ admin/store/settings/checkout/edit/panes
     * "Donation" @ admin/store/settings/orders/edit/panes
     * "Donation Incentive" @ admin/store/settings/products/edit/fields

 4. Optionally apply a default Donation Incentive to all products in your store
    (unless they specifically override it).
    @ admin/store/settings/donation_incentives

 5. Each product node will have Donation Incentive that can be set. Product
    settings override default settings.

 6. Optionally allow customers to add an additional "Top-Up" donation amount
    to their order under "Included Donation settings"
    @ admin/store/settings/checkout/edit/panes

###############################################################################
# THEMING
###############################################################################

 * Set the Donation Incentive Display Settings (incentive text and icons)
   @ admin/store/settings/donation_incentives
 * Change the location of the product incentive by adjusting the weight in
   Ubercart's "Product fields" admin @ admin/store/settings/products/edit/fields
 * For further control of themes the donation incentive indicator in node teasers
   and pages, copy product_donation_incentive.tpl.php to your theme.

###############################################################################
# MANUAL DONATION FULFILLMENT
###############################################################################

 Note: this is only required if fulfilling donations manually.

 If using Donation Incentives without the help of Benevity modules for automatic
 donation fullfillment, you will need to know how much of a donation you should
 purchase with funds from your customers' orders.

 A "Donation Incentives Report" is provided with the other Ubercart reports. This
 report lists all of the promised donations and their status (Pending or Complete).
 Pending means that the customer's  order had a donation incentive in it. The
 report tells you the donation amount on a per order basis. After you have
 fulfilled the obligation to donate, change the donation status from Pending to
 Complete by clicking the order id and scrolling to the donation pane.

###############################################################################
# AUTHOR
###############################################################################

Ryan Courtnage
Benevity Social Ventures, Inc.
ryan@benevity.org
