<?php
/* 
 * The donation indicator on a product node
 * $teaser is available
 *
 * $product_donation_incentive will contain a string:
 *   "!amount of the purchase price of this item goes to your !causeofchoice"
 * This string can be overridden in the "String overrides" section of
 * settings.php, or by using a module such as stringoverrides.module.
 *
 * The donation icon is added in uc_donation_incentives.css
 */
?>

<?php if($product_donation_incentive): ?>
  <?php if($teaser): //node teaser ?>

    <div class="product-donation-incentive-teaser">
    <a href="javascript:void(0);">
      <?php print theme('uc_donation_incentives_icon', 0); ?>
      <span><?php print $product_donation_incentive ?></span>
    </a>
    </div>

  <?php else: // node page ?>

    <div class="product-donation-incentive">
    <?php print theme('uc_donation_incentives_icon', 0); ?>
    <span><?php print $product_donation_incentive; ?></span>
    </div>

  <?php endif; ?>

<?php endif; ?>