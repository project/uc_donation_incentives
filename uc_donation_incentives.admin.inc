<?php

/**
 * Menu Callback
 */
function uc_donation_incentives_admin() {
  $output = drupal_get_form('uc_donation_incentives_default_settings');
  $output .= drupal_get_form('uc_donation_incentives_settings');
  return $output;
}

function uc_donation_incentives_default_settings($form_state) {
  $default = uc_donation_incentives_global_match_default();
  $form = array();
  $form['donation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Donation Incentive Defaults'),
    '#description' => t('These defaults will be applied to all products, and are override-able at the the catalog and product level.'),
    '#tree' => TRUE,
  );
  $form['donation']['match_type'] = array(
    '#type' => 'select',
    '#title' => t('Donation incentive type'),
    '#description' => t('How would you like to determine the donation incentive a product receives?'),
    '#options' => array(
      'none' => 'no donation',
      'fixed' => 'fixed amount',
      'sell_fraction' => 'percentage of product sell price'
    ),
    '#default_value' => $default->match_type,
  );
  $form['donation']['match_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Donation incentive rate'),
    '#description' => t('The rate at which a donation is given. For "fixed amount", enter a penny amount (ie: for $1 enter 100). For "percentage" options, enter a value from 0 to 100.'),
    '#default_value' => $default->match_rate,
  );
  $form['donation']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save'
  );
  $form['#validate'][] = 'uc_donation_incentives_match_validate';
  return $form;
}


function uc_donation_incentives_default_settings_submit(&$form, &$form_state) {
  $target_type = 'default';
  $match_type = $form_state['values']['donation']['match_type'];
  $match_rate = $form_state['values']['donation']['match_rate'];
  uc_donation_incentives_settings_save($match_type, $match_rate, $target_type);
  drupal_set_message(t('The settings have been saved.'));
}

function uc_donation_incentives_settings() {
  $form['donation_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings'),
    '#description' => t('These settings control how donation incentives appear on your site.'),
  );
  $form['donation_display']['uc_donation_incentives_cause'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination for Donations'),
    '#description' => t('Used for describing where donations go. For example: "Donations will be made to <u>your <b>Cause of Choice</b></u>."'),
    '#default_value' => variable_get('uc_donation_incentives_cause', 'your <b>Cause of Choice</b>'),
  );
  $form['donation_display']['uc_donation_incentives_icon_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Big Donation Indicator Icon Path'),
    '#description' => t('Path to image file. Leave blank to use default 22x22 icon'),
    '#default_value' => variable_get('uc_donation_incentives_icon_small', drupal_get_path('module', 'uc_donation_incentives') . '/heart22x22.png'),
  );
  $form['donation_display']['uc_donation_incentives_icon_big'] = array(
    '#type' => 'textfield',
    '#title' => t('Small Donation Indicator Icon Path'),
    '#description' => t('Path to image file. Leave blank to use default 32x32 icon'),
    '#default_value' => variable_get('uc_donation_incentives_icon_big', drupal_get_path('module', 'uc_donation_incentives') . '/heart32x32.png'),
  );
  $form['donation_display']['uc_donation_incentives_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Donation Description'),
    '#description' => t('Displayed on cart preview and checkout panes'),
    '#default_value' => variable_get('uc_donation_incentives_description', 'After you complete the checkout process, you will have the opportunity to select the causes you wish to support.'),
  );
  return system_settings_form($form);
}

function uc_donation_incentives_report() {
  if(arg(4)) {
    $status = arg(4);
  } else {
    $status = '';
  }
  $results = uc_donation_incentives_report_get($status);
  $data = array();
  while($row = db_fetch_object($results)) {
    $order = uc_order_load($row->order_id);
    if($order->order_status == 'payment_received' || $order->order_status == 'complete')
      $data[] = array(l($row->order_id, 'admin/store/orders/'.$row->order_id), check_plain(uc_currency_format($row->rebated)), check_plain(uc_currency_format($row->purchased)), $row->status, format_date($row->added_timestamp, 'custom', variable_get('uc_date_format_default', 'm/d/Y')));
  }
  $header = array(t('Order ID'), t('Rebated'), t('Purchased'), t('Status'), t('Created'));
  return theme('table', $header, $data);
}

function uc_donation_incentives_report_get($status) {
  if($status) {
    $sql = 'SELECT * FROM {uc_donation_incentives_orders} WHERE status = "%s" AND deleted_timestamp = 0 ORDER BY added_timestamp DESC';
  } else {
    $sql = 'SELECT * FROM {uc_donation_incentives_orders} WHERE deleted_timestamp = 0 ORDER BY added_timestamp DESC';
  }
  $results = db_query($sql, $status);
  return $results;
}

